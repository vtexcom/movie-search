import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Movie } from '../class/movie';
// Developer : surendra gupta
//Date : 26-Nov-2021

import data from './API-key.json'
@Injectable({
  providedIn: 'root'
})
export class OMDBService {
  
  private URL:string = `http://www.omdbapi.com/?apikey=${data.key}`
  constructor(private http:HttpClient) { }

  public getByTitle(title:string):Observable<Movie> {
    return this.http.get<Movie>(`${this.URL}&t=${title}`);
  }

  public getById(id:string):Observable<Movie> {
    return this.http.get<Movie>(`${this.URL}&i=${id}`);
  }

}
